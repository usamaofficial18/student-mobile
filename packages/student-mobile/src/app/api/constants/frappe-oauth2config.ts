export const OAuthProviderClientCredentials = {
  profileUrl:
    'https://staging-excelbd.castlecraft.in/api/method/frappe.integrations.oauth2.openid_profile',

  tokenUrl:
    'https://staging-excelbd.castlecraft.in/api/method/frappe.integrations.oauth2.get_token',

  authServerUrl: 'https://staging-excelbd.castlecraft.in',

  authorizationUrl:
    'https://staging-excelbd.castlecraft.in/api/method/frappe.integrations.oauth2.authorize',

  revocationUrl:
    'https://staging-excelbd.castlecraft.in/api/method/frappe.integrations.oauth2.revoke_token',

  scope: 'all%20openid',

  clientId: '36eeb88b4f',

  appUrl: 'excel-rma://',
};
